const mongoose = require('mongoose')

const DeveloperSchema = new mongoose.Schema({
    firstName: {
        type: String,
       required: [true,"não pode ficar vazio."]
    },
    lastName: {
        type: String,
        required: [true,"não pode ficar vazio."]
    },
    participation:{
        type: Number,
        required: [true,"não pode ficar vazio."],
        validate: {
            validator: value => value > 0 && value <= 100,
            message: 'Por favor, preencha um percentual entre 1 e 100!'
          }
    }
})

module.exports = mongoose.model('developer', DeveloperSchema);
