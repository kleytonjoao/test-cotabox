const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
//const cors = require('cors');

app = express();

const PORT = process.env.PORT || 4001;

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true}));

//connnection mongo
const db = require('../config/db.json')
const dbURI = db.connection;
mongoose.connect(dbURI, {useNewUrlParser: true});

//require models
require('../models')

//config router
app.use('/', require('../routes'));

app.listen(PORT, (err) => {
    if(err) throw err;
    console.log(`Rodando na ${PORT}`)
})

module.exports = app