const router = require('express').Router()

router.use(require('./developers'))

module.exports = router;