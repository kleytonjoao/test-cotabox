const router = require('express').Router()
const Developer = require('../models/developer');
const { map, sumBy } = require('lodash');

router.get('/', (req, res, next) => {
    res.send({ message: "Api no ar..." })
})

router.post('/developer', async (req, res, next) => {
    const { firstName, lastName, participation } = req.body;
    try {
        const developers = await Developer.find().exec()

        const developer = new Developer({ firstName, lastName, participation })
        console.log(developer)
        await developer.validate();
        const totalParticipation = sumBy(developers, 'participation') + developer.participation
        if(totalParticipation > 100) return res.status(400).send({ error: 'Total de Participção excedido!' })
        await developer.save()
        res.send({ developer })
    } catch ({errors}) {
        console.log(errors)
       res.status(400).send({error: map(errors, e=> e.message)[0]})
    }

})
module.exports = router