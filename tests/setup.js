const mongoose = require('mongoose')
const db = require('../src/config/db.json')
const dbURI = db.connectionTest;

beforeAll(done => mongoose.connect(dbURI, {useNewUrlParser: true}, done ));
afterAll(done=> mongoose.connection.dropDatabase(done));
