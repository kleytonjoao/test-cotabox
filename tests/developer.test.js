const request = require('supertest')
const app = require('../src/server/server')

const template = { firtsName: "Kleyton", lastName: "João", participation: 20 }
const templateErrorFirstName = {firtsName: "", lastName: "João", participation: 20 }
const templateErrorLastName = {firtsName: "Kleyton", lastName: "", participation: 20 }
const templateErrorParticipation = {firtsName: "Kleyton", lastName: "João", participation: "" }
const feature = '/developer'

describe('Developer Participation', () => {
    test('Name e last name should be is a pure string and percent between 1 and 100', async () => {
        const response = await request(app).post(feature).send(template)
        expect(201)
    })

    test('Developer first name is required', async () => {
        const response = await request(app).post(feature).send(templateErrorFirstName);
        expect(400)
      });

    test('Developer last name is required', async () => {
        const response = await request(app).post(feature).send(templateErrorLastName);
        expect(400)
    })

    test('Developer participation is required', async () => {
        const response = await request(app).post(feature).send(templateErrorParticipation);
        expect(400)
    })
})